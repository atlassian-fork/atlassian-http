package com.atlassian.http.url;

import com.google.common.collect.ImmutableSet;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;


import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SameOriginTest
{

    @Test
    public void testSameOriginWithMatchingOrigin() throws Exception
    {
        for (String url : ImmutableSet.of("http://e.g/2", "http://e.g:123",
            "https://e.g", "https://e.g:1235"))
        {
            String urlWithPath = url + "/extra/";
            assertThatSameOriginIs(url, urlWithPath, true);
        }
    }

    @Test
    public void testSameOriginWithMatchingOriginDifferentCasing() throws Exception
    {
        String a = "http://example.com";
        String b = "http://EXAMPLE.com";
        assertThatSameOriginIs(a, b, true);
    }

    @Test
    public void testSameOriginWithDifferentProtocol() throws Exception
    {
        String a = "http://e.g";
        String b = "https://e.g";
        assertThatSameOriginIs(a, b, false);
    }

    @Test
    public void testSameOriginWithDifferentPort() throws Exception
    {
        String a = "http://e.g:1";
        String b = "http://e.g:2";
        assertThatSameOriginIs(a, b, false);
    }

    @Test
    public void testSameOriginWithSamePort() throws Exception
    {
        String a = "http://e.g";
        String b = "http://e.g:80";
        assertThatSameOriginIs(a, b, true);
    }

    @Test
    public void testSameOriginWithDifferentHost() throws Exception
    {
        String a = "http://e.g";
        String b = "http://example.other";
        assertThatSameOriginIs(a, b, false);
    }

    @Test
    public void testSameOriginWithInvalidAuthority() throws Exception
    {
        String a = "http://e.g";
        String b = "http://example.com\\@e.g";
        assertThrows(URISyntaxException.class,
                () -> {
                    assertThatSameOriginIs(a, b, false);
                });
    }

    @Test
    public void testGetPortForUrl() throws Exception
    {
        assertThat(SameOrigin.getPortForUrl(new URL("http://a.com:12")), is(12));
        assertThat(SameOrigin.getPortForUrl(new URL("http://a.com")), is(80));
        assertThat(SameOrigin.getPortForUrl(new URL("https://a.com:12")), is(12));
        assertThat(SameOrigin.getPortForUrl(new URL("https://a.com")), is(443));
    }

    private void assertThatSameOriginIs(String a, String b, boolean expected)
        throws MalformedURLException, URISyntaxException
    {
        assertThat(SameOrigin.isSameOrigin(new URL(a), new URL(b)), is(expected));
        assertThat(SameOrigin.isSameOrigin(new URI(a), new URI(b)), is(expected));
    }
}
