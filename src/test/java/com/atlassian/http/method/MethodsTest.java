package com.atlassian.http.method;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class MethodsTest
{
    @Test
    public void testIsMutativeWithMutativeMethods()
    {
        for(String method: Arrays.asList("post", "delete", "put"))
        {
            assertIsMutative(method);
        }
    }

    @Test
    public void testIsMutativeWithNonMutativeMethods()
    {
        for(String method: Arrays.asList("get", "head", "options", "trace"))
        {
            assertIsNotMutative(method);
        }
    }

    @Test
    public void testIsMutativeIsCaseInsensitive()
    {
        for (String method: Arrays.asList("get", "GET", "Get"))
        {
            assertIsNotMutative(method);
        }
    }

    private void assertIsMutative(String method)
    {
        assertThat(method + " should be mutative",
            Methods.isMutative(method), is(true));
    }

    private void assertIsNotMutative(String method)
    {
        assertThat(method + " should not be mutative",
            Methods.isMutative(method), is(false));
    }
}
