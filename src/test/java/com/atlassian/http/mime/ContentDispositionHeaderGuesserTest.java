package com.atlassian.http.mime;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import org.apache.tika.detect.TextDetector;
import org.apache.tika.mime.MediaType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.CHROME4;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.CHROME_UNKNOWN;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.FIREFOX36;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.FIREFOX4;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.FIREFOX_UNKNOWN;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.MSIE6;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.MSIE8;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.MSIE9;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.SAFARI4;
import static com.atlassian.http.mime.UserAgentUtil.BrowserMajorVersion.SAFARI_UNKNOWN;
import static org.mockito.Matchers.any;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

public class ContentDispositionHeaderGuesserTest {
    private static final ImmutableSet<UserAgentUtil.BrowserMajorVersion> GOOD_BROWSERS = ImmutableSet.of(
            FIREFOX4, FIREFOX36, FIREFOX_UNKNOWN,
            CHROME4, CHROME_UNKNOWN,
            SAFARI4, SAFARI_UNKNOWN
    );
    private static final ImmutableSet<UserAgentUtil.BrowserMajorVersion> INTERNET_EXPLORER = ImmutableSet.of(MSIE8, MSIE9);

    private static final ImmutableSet<String> TEXT_EXTENSIONS = ImmutableSet.of(".txt");
    private static final ImmutableSet<String> TEXT_MIME_CONTENT_TYPE = ImmutableSet.of("text/plain");

    private DownloadPolicyProvider mockPolicyProvider;
    private ContentDispositionHeaderGuesser contentDispositionGuesser;
    private HostileExtensionDetector hostileExtensionDetector;
    private TextDetector textDetector;

    @BeforeEach
    public void setUp() throws Exception {
        hostileExtensionDetector = new HostileExtensionDetector();
        mockPolicyProvider = Mockito.mock(DownloadPolicyProvider.class);
        textDetector = Mockito.mock(TextDetector.class);
        contentDispositionGuesser = new ContentDispositionHeaderGuesser(
                mockPolicyProvider, hostileExtensionDetector, textDetector);
    }

    @Test
    public void testSafeExtensionsShouldUseInlineHeaderWhenSmartMode() throws Exception {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Smart);
        for (String safeExtension : ImmutableSet.of(".jpg", "png", ".gif", ".bmp")) {
            for (String contentType : ImmutableSet.of("image/bmp", "image/png", "image/gif", "image/jpeg")) {
                for (UserAgentUtil.BrowserMajorVersion browser : Iterables.concat(GOOD_BROWSERS, INTERNET_EXPLORER)) {
                    assertHeaderIs("inline", "safe" + safeExtension, contentType, browser.getUserAgentString());
                    assertMimeTypeIs(contentType, "safe" + safeExtension, contentType, browser.getUserAgentString());
                }
            }
        }
    }
    @Test
    public void testSafeFileExtensionWithNoContentTypeShouldUseAttachmentHeaderWhenSmartMode() throws Exception
    {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Smart);
        for (String safeExtension : ImmutableSet.of(".jpg", "png", ".gif", ".bmp")) {
            for (UserAgentUtil.BrowserMajorVersion browser : Iterables.concat(GOOD_BROWSERS, INTERNET_EXPLORER)) {
                assertHeaderIs("attachment", "safe" + safeExtension, "", browser.getUserAgentString());
            }
        }
    }

    @Test
    public void testSafeExtensionUnsafeContentTypeShouldUseAttachmentWhenInSmartMode() throws Exception
    {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Smart);
        for (String execContentType : hostileExtensionDetector.getExecutableContentTypes()) {
            for (UserAgentUtil.BrowserMajorVersion browser : Iterables.concat(GOOD_BROWSERS, INTERNET_EXPLORER)) {
                assertHeaderIs("attachment", "safe.txt", execContentType, browser.getUserAgentString());
            }
        }
    }

    @Test
    public void testSafeExtensionsShouldUseAttachmentHeaderWhenSecureMode() throws Exception {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Secure);
        for (String safeExtension : ImmutableSet.of(".jpg", "png", ".gif", ".bmp")) {
            for (String contentType : ImmutableSet.of("application/octet-stream", "image/bmp", "image/png", "image/gif", "image/jpeg")) {
                for (UserAgentUtil.BrowserMajorVersion browser : Iterables.concat(GOOD_BROWSERS, INTERNET_EXPLORER)) {
                    assertHeaderIs("attachment", "safe" + safeExtension, contentType, browser.getUserAgentString());
                    assertMimeTypeIs(contentType, "safe" + safeExtension, contentType, browser.getUserAgentString());
                }
            }
        }
    }

    @Test
    public void testSafeExtensionsShouldUseInlineWhenInsecureMode() throws Exception {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Insecure);
        for (String safeExtension : ImmutableSet.of(".jpg", "png", ".gif", ".bmp")) {
            for (String contentType : ImmutableSet.of("application/octet-stream", "image/bmp", "image/png", "image/gif", "image/jpeg")) {
                for (UserAgentUtil.BrowserMajorVersion browser : Iterables.concat(GOOD_BROWSERS, INTERNET_EXPLORER)) {
                    assertHeaderIs("inline", "safe" + safeExtension, contentType, browser.getUserAgentString());
                    assertMimeTypeIs(contentType, "safe" + safeExtension, contentType, browser.getUserAgentString());
                }
            }
        }
    }

    @Test
    public void testUnsafeExtensionShouldUseAttachmentHeaderWhenSmartMode() throws Exception {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Smart);
        assertUnsafeExtensionShouldUseAttachmentHeader();
    }

    @Test
    public void testUnsafeExtensionShouldUseAttachmentHeaderWhenSecureMode() throws Exception {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Secure);
        assertUnsafeExtensionShouldUseAttachmentHeader();
    }

    private void assertUnsafeExtensionShouldUseAttachmentHeader() {
        for (String contentType : hostileExtensionDetector.getExecutableContentTypes()) {
            for (String unsafeExt : hostileExtensionDetector.getExecutableFileExtensions()) {
                for (UserAgentUtil.BrowserMajorVersion browser : Iterables.concat(INTERNET_EXPLORER, GOOD_BROWSERS)) {
                    assertHeaderIs("attachment", "unsafe" + unsafeExt, contentType, browser.getUserAgentString());
                    assertMimeTypeIs(contentType, "safe" + unsafeExt, contentType, browser.getUserAgentString());
                }
            }
        }
    }

    @Test
    public void testUnsafeExtensionShouldUseAttachmentHeaderWhenInsecureMode() throws Exception {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Insecure);
        for (String contentType : hostileExtensionDetector.getExecutableContentTypes()) {
            for (String unsafeExt : hostileExtensionDetector.getExecutableFileExtensions()) {
                for (UserAgentUtil.BrowserMajorVersion browser : Iterables.concat(INTERNET_EXPLORER, GOOD_BROWSERS)) {
                    assertHeaderIs("inline", "unsafe" + unsafeExt, contentType, browser.getUserAgentString());
                    assertMimeTypeIs(contentType, "safe" + unsafeExt, contentType, browser.getUserAgentString());
                }
            }
        }
    }

    @Test
    public void testSpecialCaseIEHandlingForTextExtensionsWhenSmartMode() throws Exception {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Smart);
        for (String textExtension : TEXT_EXTENSIONS) {
            for (String textMimeType : TEXT_MIME_CONTENT_TYPE) {
                for (UserAgentUtil.BrowserMajorVersion browser : INTERNET_EXPLORER) {
                    assertHeaderIs("attachment", "safe" + textExtension, textMimeType, browser.getUserAgentString());
                    assertHeaderIs("attachment", "safe" + ".bogus", textMimeType, browser.getUserAgentString());
                    assertMimeTypeIs(textMimeType, "safe" + textExtension, textMimeType, browser.getUserAgentString());
                }

                for (UserAgentUtil.BrowserMajorVersion browser : GOOD_BROWSERS) {
                    assertHeaderIs("inline", "safe" + textExtension, textMimeType, textMimeType);
                    assertMimeTypeIs("text/plain", "safe" + textExtension, textMimeType, browser.getUserAgentString());
                }
            }
        }
    }

    @Test
    public void testSpecialCaseIEHandlingForTextExtensionsWhenSecureMode() throws Exception {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Secure);
        for (String textExtension : TEXT_EXTENSIONS) {
            for (String textMimeType : TEXT_MIME_CONTENT_TYPE) {
                for (UserAgentUtil.BrowserMajorVersion browser : Iterables.concat(INTERNET_EXPLORER, GOOD_BROWSERS)) {
                    assertHeaderIs("attachment", "safe" + textExtension, textMimeType, browser.getUserAgentString());
                    assertMimeTypeIs(textMimeType, "safe" + textExtension, textMimeType, browser.getUserAgentString());
                }
            }
        }
    }

    @Test
    public void testSpecialCaseIEHandlingForTextExtensionsWhenInsecureMode() throws Exception {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Insecure);
        for (String textExtension : TEXT_EXTENSIONS) {
            for (String textMimeType : TEXT_MIME_CONTENT_TYPE) {
                for (UserAgentUtil.BrowserMajorVersion browser : Iterables.concat(INTERNET_EXPLORER, GOOD_BROWSERS)) {
                    assertHeaderIs("inline", "safe" + textExtension, textMimeType, browser.getUserAgentString());
                    assertMimeTypeIs(textMimeType, "safe" + textExtension, textMimeType, browser.getUserAgentString());
                }
            }
        }
    }

    @Test
    public void testMatchHostileContentTypeInGivenExecutableContentType()
    {
        List <String> modifiers = Arrays.asList("", " ", "  ", "\0", "    ", "      ");
        for (String contentType : hostileExtensionDetector.getExecutableContentTypes()) {
            for (String modifier : modifiers) {
                assertTrue(hostileExtensionDetector.isExecutableContentType(modifier + contentType + modifier) );
            }
        }
    }

    @Test
    public void testInvalidContentType()
    {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Smart);
        final List<String> executableTypes = Arrays.asList(
                "1337",
                "13;x/37",
                "13;x/37",
                "123123;/123",

                // Browsers consider 'text/ html' as 'text/html'
                // It means text/ html' should be executable as well
                "text/html",
                "text/ html",
                "text /html",
                "text / html",

                "text/html;charset=UTF-8",
                "text/ html;charset=UTF-8",
                "text /html;charset=UTF-8",
                "text / html;charset=UTF-8",

                "text/html; charset=UTF-8",
                "text/ html; charset=UTF-8",
                "text /html; charset=UTF-8",
                "text / html; charset=UTF-8",

                // Despite "text/plain" is not the executable content type,
                // it becomes invalid (and executable) when contains spaces
                "text/ plain",
                "text /plain",
                "text / plain",
                "text/ plain;charset=UTF-8",
                "text /plain;charset=UTF-8",
                "text / plain;charset=UTF-8",
                "text/ plain; charset=UTF-8",
                "text /plain; charset=UTF-8",
                "text / plain; charset=UTF-8"
        );

        for (String contentType : executableTypes) {
            assertTrue(hostileExtensionDetector.isExecutableContentType(contentType));
        }
        assertFalse(hostileExtensionDetector.isExecutableContentType("text/plain"));
        assertFalse(hostileExtensionDetector.isExecutableContentType("text/plain; charset=UTF-8"));
        assertFalse(hostileExtensionDetector.isExecutableContentType("text/plain;charset=UTF-8"));
    }

    @Test
    public void testNoMimeTypeNoExtension() {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Smart);
        for (UserAgentUtil.BrowserMajorVersion browser : Iterables.concat(INTERNET_EXPLORER, GOOD_BROWSERS)) {
            assertHeaderIs("attachment", "", null, browser.getUserAgentString());
        }
    }

    @Test
    public void testNewGuessMIMEWhenSmartMode()
    {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Smart);
        assertEquals("somethingbinary", contentDispositionGuesser.guessMIME("somethingbinary", true) );
        assertEquals("text/plain", contentDispositionGuesser.guessMIME("somethingtext", false) );
    }

    @Test
    public void testNewGuessMIMEWhenNotInSmartMode()
    {
        List<DownloadPolicy> policies = Arrays.asList(DownloadPolicy.Secure,
                DownloadPolicy.Insecure, DownloadPolicy.WhiteList);
        for (DownloadPolicy policy : policies)
        {
            when(mockPolicyProvider.getPolicy()).thenReturn(policy);
            assertEquals("somethingbinary", contentDispositionGuesser.guessMIME("somethingbinary", true) );
            assertEquals("somethingtext", contentDispositionGuesser.guessMIME("somethingtext", false) );
        }
    }

    @Test
    public void testIsBinaryWithBinary() throws IOException {
        when(textDetector.detect(any(), any())).thenReturn(MediaType.OCTET_STREAM);
        assertTrue(contentDispositionGuesser.isBinary(null));
    }

    @Test
    public void testIsBinaryWithNonBinary() throws IOException {
        when(textDetector.detect(any(), any())).thenReturn(MediaType.TEXT_HTML);
        assertFalse(contentDispositionGuesser.isBinary(null));
    }

    @Test
    public void testDetermineHeadersFromContentInWhitelistModeWithBinary() throws IOException {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.WhiteList);
        setTextDetectorToDetectBinary();
        final String mediaType = hostileExtensionDetector.getSafeContentTypes()
                .iterator().next();
        Map<String, String> expected = new DownloadHeaderHelper(
                "inline", mediaType).getDownloadHeaders();
        assertDetermineHeadersFromContentHeaders(mediaType, expected, FIREFOX36.getUserAgentString());
    }

    @Test
    public void testDetermineHeadersFromContentInWhitelistModeWithNonSafeBinary()
            throws IOException {
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.WhiteList);
        setTextDetectorToDetectBinary();
        final String mediaType = "audio/something-unsafe";
        Map<String, String> expected = new DownloadHeaderHelper(
                "attachment", mediaType).getDownloadHeaders();
        assertDetermineHeadersFromContentHeaders(mediaType, expected, FIREFOX36.getUserAgentString());
    }

    @Test
    public void testDetermineHeadersFromContentInWhitelistModeInIe8() throws IOException {
        /* in ie < ie8 the content-disposition should be attachment */
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.WhiteList);
        setTextDetectorToDetectBinary();
        final String userAgent = MSIE6.getUserAgentString();
        final String safeMediaType = hostileExtensionDetector.getSafeContentTypes()
                .iterator().next();
        for (String mediaType: ImmutableSet.of(safeMediaType, "video/something-unsafe")) {
            Map<String, String> expected = new DownloadHeaderHelper(
                    "attachment", mediaType).getDownloadHeaders();
            assertDetermineHeadersFromContentHeaders(mediaType, expected, userAgent);
        }
    }

    @Test
    public void testDetermineHeadersFromContentInNonWhitelistMode()
            throws IOException {
        List<DownloadPolicy> policies = Arrays.asList(DownloadPolicy.Secure,
                DownloadPolicy.Smart);
        String mediaType = "text/html";
        String userAgent = FIREFOX36.getUserAgentString();
        for (DownloadPolicy policy : policies) {
            when(mockPolicyProvider.getPolicy()).thenReturn(policy);
            Map<String, String> expected = new DownloadHeaderHelper(
                    "attachment", mediaType).getDownloadHeaders();
            assertDetermineHeadersFromContentHeaders(mediaType, expected, userAgent);
        }
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Insecure);
        Map<String, String> expected = new DownloadHeaderHelper(
                "inline", mediaType).getDownloadHeaders();
        assertDetermineHeadersFromContentHeaders(mediaType, expected, userAgent);
    }

    @Test
    public void testDetermineHeadersFromContentInSmartModeWithIe8()
            throws IOException {
        String mediaType = "text/plain";
        String userAgent = UserAgentUtilImplTest.IE_8_UA;
        when(mockPolicyProvider.getPolicy()).thenReturn(DownloadPolicy.Smart);
        Map<String, String> expected = new DownloadHeaderHelper(
                "inline", mediaType).getDownloadHeaders();
        assertDetermineHeadersFromContentHeaders(mediaType, expected, userAgent);
    }

    private void assertDetermineHeadersFromContentHeaders(
            String contentType, Map<String, String> expectedheaders, String userAgent) {
        Map<String, String> headers = contentDispositionGuesser.determineHeadersFromContent(
                "", contentType, userAgent, null);
        assertEquals(expectedheaders, headers);
    }

    private void setTextDetectorToDetectBinary() throws IOException {
        when(textDetector.detect(any(), any())).thenReturn(MediaType.OCTET_STREAM);
    }

    private void assertMimeTypeIs(String expectedMimeReturned, String filename, String givenMimeType, String browserUserAgent) {
        final String message = "wrong header for filename=" + filename + " and mime=" + givenMimeType + " and userAgent=" + browserUserAgent;
        assertEquals(
                expectedMimeReturned,
                contentDispositionGuesser.guessMIME(filename, givenMimeType, browserUserAgent),
                message);
    }

    private void assertHeaderIs(final String expectedHeader, final String extension, final String mimeContentType, final String userAgentStr) {
        final String message = "wrong header for extension=" + extension + " and mime=" + mimeContentType + " and userAgent=" + userAgentStr;
        assertEquals(
                expectedHeader,
                contentDispositionGuesser.guessContentDispositionHeader(
                        extension, mimeContentType, userAgentStr),
                message);
    }
}
