package com.atlassian.http.mime;

/**
 * Provides a way to get the policy.
 */
public interface DownloadPolicyProvider {
    /**
     * @return the download policy. Implement this method to integrate with the configuration of your application
     */
    DownloadPolicy getPolicy();
}
