package com.atlassian.http.mime;

/**
 * Possible policies
 */
public enum DownloadPolicy {
    /**
     * Allow all raw to be viewed in the browser directly.
     */
    Insecure,
    /**
     * Forces non-secure file types (i.e., executable file types) to be downloaded instead of viewable in brwoser.
     * Security of file types determined by {@link HostileExtensionDetector}.
     */
    Smart,
    /**
     * Uses a whitelist to determine which files should be downloaded instead of viewable
     * in the browser.
     */
    WhiteList,
    /**
     * All files are forced to download.
     */
    Secure;

}
