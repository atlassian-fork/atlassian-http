package com.atlassian.http.mime;


class StringUtils {

    static boolean isBlank(String string) {
        return string == null || string.trim().isEmpty();
    }

}
