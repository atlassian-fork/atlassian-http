package com.atlassian.http.method;

import com.google.common.collect.ImmutableSet;

import java.util.Locale;

/**
 * This class contains functions that can be used to obtain information
 * about http methods.
 *
 * @since 1.0.10.
 */
public class Methods
{
    public static boolean isMutative(String method)
    {
        if (ImmutableSet.of("GET", "HEAD", "OPTIONS", "TRACE")
            .contains(method.toUpperCase(Locale.US)))
        {
            return false;
        }
        return true;
    }
}
